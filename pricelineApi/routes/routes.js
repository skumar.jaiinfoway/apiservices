var express = require('express');
var router = express.Router();
import * as prlController from "../controller/pricelineController";

router.get("/search_hotal", function (req, res) {
    prlController.searchHotel(req, res);
});

router.get("/book_hotal", function (req, res) {
    prlController.bookHotel(req, res);
});

router.get("/view_status", function (req, res) {
    prlController.viewStatus(req, res);
});

router.get("/cancle_booking", function (req, res) {
    prlController.cancleBooking(req, res);
});

module.exports = router;