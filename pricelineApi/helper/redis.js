const redis = require("redis");
const radisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_URL);
const { promisify } = require('util');
const redisGetAsync = promisify(radisClient.get).bind(radisClient);

export const redisSet = function (key, value, ttl = 600) {
    radisClient.setex(key, ttl, value);
}
export const redisGet = async function (cacheKey) {
    var result = await redisGetAsync(cacheKey);
    return result;
}
