var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes/routes.js");
var app = express();
const path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static('public'));

// Alt
app.use('/', routes);
app.use((req, res, next) => {
    res.status(404).send('<h1> Page not found </h1>');
});

// routes(app);

var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port);
});