import { redisGet, redisSet } from "../helper/redis";
import * as Search from "../lib/search";
import * as Booking from "../lib/booking";
import * as Cancle from "../lib/cancle";
import * as Status from "../lib/status";

export const searchHotel = async (req, res) => {
    let hotelData = '';
    try {
        let searchParams = await Search.getSearchParams();
        // searchParams response an array, for testing select one of them as New Delhi
        searchParams.cities.forEach(async (item, index) => {
            if (item.city == 'New Delhi') {
                // Radis cache key
                const cacheKey = `pipeline_${item.cityid_ppn}${'2021-06-22'}${'2021-06-23'}`;
                // // Get result from cache
                var result = await redisGet(cacheKey);
                
                if (result)
                    return res.send({ "status": 200, "error": false, "response": JSON.parse(result) });

                hotelData = await Search.getHotels(item);
                // Parse result
                var parsedValue = hotelData;
                // // Cache result to radis for 1 hr
                await redisSet(cacheKey, JSON.stringify(parsedValue), 600);

                return res.send({ "status": 200, "error": false, "response": parsedValue });
                // res.send({ "status": 200, "error": false, "response": hotelData });
            }

        })
    } catch (err) {
        var msg = err.message;
        if (err.response && err.response.data)
            msg = err.response.data.message;
        return res.status(404).json({ status: false, message: msg });
    }
}

export const bookHotel = async (req, res) => {
    let ppn_buldle = 'HER_DqWBik9kwVr6R92su8z_a6X3PRO0LNBLwD4SVX4tJhAgBV1XQxfIXvBkoYbxj5yPLmzX2PNnG0i8Q_Ac8h03Tpx-RMGECMaPqf-WkuxnNYvYl-qP7w9gtnwnB4BX41inTODL5jSm0Qcnc7dGWbT2LZ45RHq2TWIR1-Ra0BZ6N5sqcZlOWSm_No3bWwWK8NoD3dbJhZAHQWv0moZE6k4E8wYXocI4yR-Bx5Vz-g-9CGZ6wL6f6ENQdsAuBtlN7wVhP4_bFD5kzcOZE2Te55pu-3nLyy1QKVHcEdBxWphisQhadfvsm3wfcdTwBzMx6cArTnumQXwMRsCwrSrt6eC6ZPFDVNULLZwSwyx5upM2Zjgi7ZhJjOLzbTNbq1hd59JZ1nP9kZPokxpRgbxzohwFH7grQU8lDMx5fA5WVWYBX0yLjPpOFCDZzLlEvGl1PcSjXo9sY4B1QD5-1GQznK6LeSLKBbUgI_DQQ-lojCvoXPLwgpNrcgMGhs4k0amrshXmV-z-QUgWh7RUhncsdwK6_P2F_cmjvU2pjjjhWMwNS6NV65flLY0jHqaf1j3SmfLxRMoAaA0XeqzFuEu1sH4IdHKWtKN5Nheeya68kEq0DWKekEvXonFS_yHy0GTLp7rr3HSsv9o_f4qpmDJr85BikY63QW42_CO5I_BpWLuNRvM2Fink14-OS1U6-TTwFIh-R3iLYiD80CnJdoDHpASIidRo7l2HnvCS5VLjeaGVZsuOynYsXIgQ7UAwOz7x-h_u6xuHwHImvvYhnC043CmQ2sTpYQbtzHaMxZzzSsdJ5dfmhEVVm4f1-7TyxGooTyS6qD5hdPtht74AxBf7A0gU2HbvjRpZCfe1PawE9oPBpefZDVkLekvtEKVT8hMno3zwSoM0YWcR-gdq7UKUuBDqSj6PBxARc3BEsvTPPRLj_-f7CkOMn1BY3J0d19INk7b8B_C8j986KdMxCcc6uRVtfBRjD3eeHyt0zK3bighfhC2hwrPpgndgUZAv0SIwQhAsFGRfQZg2vYGKPei3CBg7_ilC4gaQmymLUjgcrqGd-xJ6yS__bmPzGvWq0HDI-yYtBQyaai0AIHw9ydFGYQuN7WofcWzBsg0RgYFvhU3yYhNuP0R5kaTmY7hi_k79KP-VoyMYAeOqvVx1emhcxngnQ_lZIla_it7Z8sTwOR9Whz9pmjzNa21giCkImjqOXDxBjHkKuzakFJ5FWR3AKQNCWFb8r9qP4xyVNYA0pH7sBUEcQr1c6w1T1rO7Ae1aoDel';
    try {
        let contractData = await Booking.getContract(ppn_buldle);
        // console.log(contractData)
        let bookData = await Booking.bookHotel(contractData.ppn_book_bundle);
        // console.log(bookData)
        res.send({ "status": 200, "error": false, "response": bookData });

    } catch (err) {
        var msg = err.message;
        if (err.response && err.response.data)
            msg = err.response.data.message;
        return res.status(404).json({ status: false, message: msg });
    }
}

export const viewStatus = async (req, res) => {
    try {
        let lookupData = await Status.getLookUp();
        // console.log(cancleData)
        res.send({ "status": 200, "error": false, "response": lookupData });

    } catch (err) {
        var msg = err.message;
        if (err.response && err.response.data)
            msg = err.response.data.message;
        return res.status(404).json({ status: false, message: msg });
    }
}

export const cancleBooking = async (req, res) => {
    let hotelData = '';
    try {
        let lookupData = await Cancle.getLookUp();
        let cancleData = await Cancle.cancleBooking(lookupData);
        // console.log(cancleData)
        res.send({ "status": 200, "error": false, "response": cancleData });

    } catch (err) {
        var msg = err.message;
        if (err.response && err.response.data)
            msg = err.response.data.message;
        return res.status(404).json({ status: false, message: msg });
    }
}
