const axios = require('axios');
const qs = require('qs');

export const getSearchParams = async function () {
    try {
        const response = await axios({
            method: 'GET',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getAutoSuggestV2?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726&string=delhi',
        });
        if (response.data.getHotelAutoSuggestV2.results != undefined)
            return response.data.getHotelAutoSuggestV2.results.result;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}

export const getHotels = async function (searchData) {
    try {
        const response = await axios({
            method: 'GET',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getExpress.Results?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726&city_id=' + searchData.cityid_ppn + '&check_in=2021-06-18&check_out=2021-06-19',
        });
        // console.log(response.data['getHotelExpress.Results'].results.hotel_data)
        if (response.data['getHotelExpress.Results'].results != undefined)
            return response.data['getHotelExpress.Results'].results.hotel_data;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}