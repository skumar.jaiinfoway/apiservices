const axios = require('axios');
const qs = require('qs');

export const getContract = async function (ppn_buldle) {
    try {
        // console.log(hotelData.room_data[0].rate_data[0].ppn_bundle);
        const response = await axios({
            method: 'GET',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getExpress.Contract?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726&ppn_bundle=' + ppn_buldle,
        });
        if (response.data['getHotelExpress.Contract'].results != undefined)
            return response.data['getHotelExpress.Contract'].results.bundle_data;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}

export const bookHotel = async function (ppn_book_bundle) {
    try {
        // console.log(hotelData.room_data[0].rate_data[0].ppn_bundle);
        const response = await axios({
            method: 'POST',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getExpress.Book?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726',
            data: qs.stringify({
                'name_first': 'testing1',
                'name_last': 'testing2',
                'phone_number': '9876543210',
                'ppn_bundle': ppn_book_bundle,
                'email': 'abc@abc.abc',
                'card_type': 'VI',
                'card_number': '4111111111111111',
                'expires': '022022',
                'card_holder': 'tester',
                'address_line_one': 'testing address',
                'address_city': 'testing city',
                'address_postal_code': '831001',
                'country_code': 'IN'
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        });
        // console.log(response.data)
        if (response.data['getHotelExpress.Book'].results != undefined)
            return response.data['getHotelExpress.Book'].results;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}
