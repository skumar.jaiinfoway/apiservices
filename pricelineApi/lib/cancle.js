const axios = require('axios');
const qs = require('qs');

export const getLookUp = async function () {
    try {
        // console.log(hotelData.room_data[0].rate_data[0].ppn_bundle);
        const response = await axios({
            method: 'POST',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getExpress.LookUp?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726',
            data: qs.stringify({
                'booking_id': '49012437181',
                'email': 'abc@abc.abc'
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        });
        // console.log(response.data)
        if (response.data['getHotelExpress.LookUp'].results != undefined)
            return response.data['getHotelExpress.LookUp'].results.result.actions.resend_itinerary;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}

export const cancleBooking = async function (ppn_bundle) {
    try {
        const response = await axios({
            method: 'POST',
            url: 'https://api-sandbox.rezserver.com/api/hotel/getExpress.Cancel?format=json2&refid=10046&api_key=990b98b0a0efaa7acf461ff6a60cf726&ppn_bundle=' + ppn_bundle,
        });
        console.log(response.data)
        if (response.data['getHotelExpress.Cancel'].results != undefined)
            return response.data['getHotelExpress.LookUp'].results;
        else
            return '';
    }
    catch (err) {
        var msg = err.message;

        if (err.response && err.response.data)
            msg = JSON.parse(Buffer.from(err.response.data).toString('utf8')).message;

        throw new Error(msg);
    }
}
